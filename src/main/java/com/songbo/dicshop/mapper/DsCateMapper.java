package com.songbo.dicshop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.songbo.dicshop.entity.DsCate;


public interface DsCateMapper extends BaseMapper<DsCate> {
}