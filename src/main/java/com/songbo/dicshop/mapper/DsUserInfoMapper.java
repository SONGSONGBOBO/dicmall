package com.songbo.dicshop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.songbo.dicshop.entity.DsUserInfo;


public interface DsUserInfoMapper extends BaseMapper<DsUserInfo> {
}