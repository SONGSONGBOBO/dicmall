package com.songbo.dicshop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.songbo.dicshop.entity.DsUser;


public interface DsUserMapper extends BaseMapper<DsUser> {
}