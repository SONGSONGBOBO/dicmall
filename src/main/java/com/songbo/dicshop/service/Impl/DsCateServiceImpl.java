package com.songbo.dicshop.service.Impl;

import com.songbo.dicshop.entity.DsCate;
import com.songbo.dicshop.service.DsCateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName DsCateServiceImpl
 * @Description TODO
 * @Author songbo
 * @Date 2020/2/13 下午10:01
 **/
@Service
@Slf4j
public class DsCateServiceImpl implements DsCateService {
    @Override
    public boolean saveCate(DsCate dsCate) {
        return false;
    }

    @Override
    public boolean updateCates(String userId) {
        return false;
    }
}
